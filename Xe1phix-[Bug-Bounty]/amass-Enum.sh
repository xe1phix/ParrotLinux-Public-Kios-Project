#!/bin/bash

# OSINT Recon and Enumeration Script using Amass
# ==============================================
# This script performs OSINT recon using Amass to enumerate subdomains,
# and also includes passive DNS enumeration and other checks.

# Ensure Amass is installed
check_amass() {
    if ! command -v amass &> /dev/null; then
        echo "[-] Amass is not installed. Please install it first."
        exit 1
    fi
}

# Function to gather subdomains using Amass
enumerate_subdomains() {
    echo "========== SUBDOMAIN ENUMERATION =========="
    echo "[+] Starting Amass subdomain enumeration..."
    amass enum -passive -d "$domain" -o "$output_dir/subdomains.txt"
    echo "[+] Subdomains saved to: $output_dir/subdomains.txt"
    echo "-------------------------------------------"
}

# Function to perform reverse DNS enumeration
reverse_dns() {
    echo "========== REVERSE DNS LOOKUP =========="
    if [ -s "$output_dir/subdomains.txt" ]; then
        echo "[+] Performing reverse DNS lookup for identified subdomains..."
        while read -r subdomain; do
            dig +short "$subdomain" | while read ip; do
                if [[ -n "$ip" ]]; then
                    echo "$subdomain -> $ip"
                    echo "$subdomain -> $ip" >> "$output_dir/reverse_dns.txt"
                fi
            done
        done < "$output_dir/subdomains.txt"
        echo "[+] Reverse DNS results saved to: $output_dir/reverse_dns.txt"
    else
        echo "[-] No subdomains found. Skipping reverse DNS lookup."
    fi
    echo "-------------------------------------------"
}

# Function to gather WHOIS information
whois_lookup() {
    echo "========== WHOIS INFORMATION =========="
    echo "[+] Performing WHOIS lookup for $domain..."
    whois "$domain" > "$output_dir/whois.txt"
    echo "[+] WHOIS information saved to: $output_dir/whois.txt"
    echo "----------------------------------------"
}

# Function to check DNS records
dns_records() {
    echo "========== DNS RECORDS =========="
    echo "[+] Fetching DNS records for $domain..."
    dig ANY "$domain" +noall +answer > "$output_dir/dns_records.txt"
    echo "[+] DNS records saved to: $output_dir/dns_records.txt"
    echo "---------------------------------"
}

# Function to resolve identified subdomains to IP addresses
resolve_subdomains() {
    echo "========== SUBDOMAIN IP RESOLUTION =========="
    if [ -s "$output_dir/subdomains.txt" ]; then
        echo "[+] Resolving subdomains to IP addresses..."
        while read -r subdomain; do
            ip=$(dig +short "$subdomain" | head -n 1)
            if [[ -n "$ip" ]]; then
                echo "$subdomain -> $ip"
                echo "$subdomain -> $ip" >> "$output_dir/subdomains_ips.txt"
            fi
        done < "$output_dir/subdomains.txt"
        echo "[+] Subdomain IPs saved to: $output_dir/subdomains_ips.txt"
    else
        echo "[-] No subdomains found. Skipping subdomain IP resolution."
    fi
    echo "--------------------------------------------"
}

# Function to generate a summary report
generate_report() {
    echo "========== SUMMARY REPORT =========="
    echo "[+] Generating summary report..."
    {
        echo "Subdomains found:"
        cat "$output_dir/subdomains.txt"
        echo
        echo "Subdomain IP resolution:"
        cat "$output_dir/subdomains_ips.txt"
        echo
        echo "Reverse DNS results:"
        cat "$output_dir/reverse_dns.txt"
        echo
        echo "WHOIS Information:"
        cat "$output_dir/whois.txt"
        echo
        echo "DNS Records:"
        cat "$output_dir/dns_records.txt"
    } > "$output_dir/summary_report.txt"
    echo "[+] Summary report saved to: $output_dir/summary_report.txt"
    echo "-----------------------------------"
}

# Main function to run the entire OSINT process
run_osint_recon() {
    check_amass
    mkdir -p "$output_dir"

    enumerate_subdomains
    reverse_dns
    whois_lookup
    dns_records
    resolve_subdomains
    generate_report

    echo "========== OSINT RECON COMPLETE =========="
}

# Check if the domain argument is passed
if [ $# -eq 0 ]; then
    echo "[-] No domain provided. Usage: $0 <domain>"
    exit 1
fi

# Variables
domain="$1"
output_dir="amass_recon_$domain"

# Run the OSINT Recon process
run_osint_recon